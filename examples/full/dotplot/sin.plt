reset
set terminal epslatex size 4,2.5
set output 'plt/sin.tex'

set xlabel '$x$'
set ylabel '$y$'
set xrange [-10:10]
set yrange[-1.2:1.2]

set key t r
plot sin(x) title 'Ein Lurch auf dem Heimweg' 

set output
