# coding: utf-8
import numpy as np
import matplotlib
matplotlib.use('pgf')
texsettings = {
    "text.usetex": True,    # use inline math for ticks
    "pgf.texsystem": "lualatex"
}
matplotlib.rcParams.update(texsettings)
import matplotlib.pyplot as plt


fig, axes = plt.subplots(figsize=(4,3))
axes.set_ylabel('Zeit(s)')
axes.set_xlabel('Auslenkung')
x=np.linspace(-10,10,100)
axes.plot(x, np.cos(x), '--.', label='Pendel 1')
axes.legend()


plt.tight_layout(.5)
plt.savefig("plt/cos.pgf")
