# Labreport LaTeX-Style für F-Praktikumsberichte
## Features:
- Sehr minimalistische Titelseite
- Gebaut für LuaLaTeX (UTF-8)
- .sty anstelle von .cls
- Overleaf/Sharelatex-Vorlage

## Beispiele:

- Minimal: `cd examples/minimal` und `lualatex main.tex` - fertig.
- Full: Erst wie im folgenden Abschnitt beschrieben labreport installieren, dann ins Verzeichnis `examples/full` wechseln und `make` aufrufen.
- Overleaf/Sharelatex-Vorlage: overlatex.zip hochladen.

## Installation:
Es gibt verschiedene Varianten:

- Schnell und einfach: `ln -s pfad/zu/labreport/* .` aus dem Arbeitsverzeichnis aufrufen.
- Eleganter, benutzerweit: Den Ordner labreport in das TeXMF-Verzeichnis kopieren (i.d.R. ~/texmf/tex/latex, unter Linux herauszufinden mit `kpsewhich -var-value=TEXMFHOME`)
- Alternativ, für einzelne Projekte: `export TEXINPUTS` nutzen, um das labreport-Verzeichnis bekannt zu machen. -> Google
